<?php

namespace Bueno\Beer;
use Bueno\Beer\ArrayHelper;
use Bueno\Beer\Cerveja;
use Bueno\Beer\Notes;
use Bueno\Beer\Price;

interface Ranquear
{
    public function sort_asc(array $cerveja);

    public function sort_desc(array $cerveja);
}




