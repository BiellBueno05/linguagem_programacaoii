<?php

namespace Bueno\Beer;
use Bueno\Beer\Ranquear;
use Bueno\Beer\ArrayHelper;
use Bueno\Beer\Cerveja;

class Price implements Ranquear
{   



    public function sort_asc(array $cerveja) 
    {

        return ArrayHelper::array_sort($cerveja, 'preço', SORT_ASC);

    }

    public function sort_desc(array $cerveja)
    {

        return ArrayHelper::array_sort($cerveja, 'preço', SORT_DESC);

    }
    
}
