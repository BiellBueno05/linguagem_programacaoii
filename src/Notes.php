<?php

namespace Bueno\Beer;
use Bueno\Beer\Ranquear;
use Bueno\Beer\ArrayHelper;
use Bueno\Beer\Cerveja;

Class Notes implements Ranquear

{


    public function sort_asc(array $cerveja)
    {

        return ArrayHelper::array_sort($cerveja, 'nota', SORT_ASC);

    }

    public function sort_desc(array $cerveja)
    {

        return ArrayHelper::array_sort($cerveja, 'nota', SORT_DESC);

    }
    
}