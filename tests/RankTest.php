<?php

// src/project-01/php/tests/HelloTest.php 

use PHPUnit\Framework\TestCase;
use Bueno\Beer\ArrayHelper;
use Bueno\Beer\Cerveja;
use Bueno\Beer\Notes;
use Bueno\Beer\Price;


final class RankTest extends TestCase
{
    public function testNotes() 
    {
        $array1 = [
            'skol' => array('estado' =>'SP',
            'preço' => 4.90,
            'nota'  => 5),

            'Brahma' => array('estado' =>'RJ',
            'preço' => 7.90,
            'nota'  => 3),

            'Local' => array('estado' =>'SC',
            'preço' => 5.90,
            'nota'  => 4),
        ]; 
        $arraySort = [
            'skol' => array('estado' =>'SP',
            'preço' => 4.90,
            'nota'  => 5),

            'Local' => array('estado' =>'SC',
            'preço' => 5.90,
            'nota'  => 3),

            'Brahma' => array('estado' =>'RJ',
            'preço' => 7.90,
            'nota'  => 4),
        ];

        
        $rankPriceAsc =  new Price();
        $rankPriceAsc->sort_asc($array1);

        $this->assertEqualsCanonicalizing($rankPriceAsc,$arraySort);
    }
}
