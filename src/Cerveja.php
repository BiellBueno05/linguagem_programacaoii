<?php

namespace Bueno\Beer;

class Cerveja{

    public $cerveja;

    public function persistenData()
    {
        $this->cerveja = [

            "skol" => array('estado' =>'SP',
                            'preço' => 4.90,
                            'nota'  => 5),

            "Brahma" => array('estado' =>'RJ',
                            'preço' => 3.90,
                            'nota'  => 3),

            "Local" => array('estado' =>'SC',
                            'preço' => 1.90,
                            'nota'  => 2),


        ];
    }

    public function show()
    {

        return $this->cerveja;
    
    }
}